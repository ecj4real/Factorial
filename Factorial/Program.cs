﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number: ");
            int value = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("The factorial of " + value + " is: " + factor(value));

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        public static long factor(int a)
        {
            if (a <= 1)
            {
                return 1;
            }
            else
            {
                return a * factor(a - 1);
            }
        }
    }
}
